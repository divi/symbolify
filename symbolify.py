#!/usr/bin/env python3

import sys
from  PIL import Image

if len(sys.argv) < 3:
    print(f'Usage: {sys.argv[0]} input_file(gif) output_file(basic)')
    sys.exit()


font_file = Image.open(sys.argv[1])

smallest = 255
symbols = []
width_table = [0] * 256

for char_x in range(16):
    for char_y in range(16):

        is_char = False
        current_char = []
        char_code = char_y + 16 * char_x

        for y in range(8):
            current_line = 0

            for x in range(8):
                if font_file.getpixel((x + 8 * char_x, y + 8 * char_y)) != 0:
                    current_line += 1 << (7 - x)
                    is_char = True

                    if width_table[char_code] <= x: width_table[char_code] = x + 1

            current_char.append(current_line)

        if is_char:

            if smallest > char_code: smallest = char_code

            symbols.append([char_code, current_char[:]])

with open(sys.argv[2], 'w') as basic_file:
    line_counter = 50000
    basic_file.write(f'{line_counter} SYMBOL AFTER &{smallest:02X}\n')
    for charac in symbols:
        line_counter += 10
        symbol_string = f'{line_counter} SYMBOL &{charac[0]:02X}'
        for line in range(8):
            symbol_string += f',&{charac[1][line]:02X}'
        basic_file.write(symbol_string + '\n')

    line_counter += 10
    basic_file.write(f'{line_counter} RETURN\n')

    line_counter += 10
    basic_file.write(f'{line_counter} \'\n')
    line_counter += 10
    basic_file.write(f'{line_counter} \' ** Characters width data\n')
    line_counter += 10
    basic_file.write(f'{line_counter} \'\n')

    for w in range(0, 256, 16):
        line_counter += 10
        width_string = f'{line_counter} DATA '
        for d in range(16):
            width_string += f'{width_table[w + d] if width_table[w + d] > 0 else 8},'
        width_string = width_string[:len(width_string)-1] + '\n'
        basic_file.write(width_string)
